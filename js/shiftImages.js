let classElemShiftImages = "page-float-end";

class shiftImages extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.token;
  }


  // i changed the afterParsed to the beforeParsed to have a clean template to work with.
  // After Parsed means that the content is well changed. we should avoid it.
  
  beforeParsed(content) {
    //replace target here whenever necessary
    let tar = '.component-body'
    
    let target = content.querySelector(`${tar}`);
    let section = document.createElement('section');

    section.classList.add('imageToEnd')


    content.querySelectorAll(`.${classElemShiftImages}`).forEach((element, index) => {
      // console.log(`element`, element)
      // console.log(`section`, section)
      // console.log(`target`, target)

      let id = element.id
      let caption = content.querySelector(`#${id} figcaption`).textContent;

      let a = document.createElement('a');
      let link = document.createTextNode(`(See image ${index+1}: ${caption})`)
      a.appendChild(link)
      a.href = `#${id}`
      a.classList.add('linkImgEnd')
      element.insertAdjacentElement('beforebegin', a)
      section.insertAdjacentElement('beforeend', element);

    });


    target.insertAdjacentElement('afterend', section);
  }

}

Paged.registerHandlers(shiftImages);

// This below is not needed, as we don’t need the image size/ratio

// async function awaitImageLoaded(image) {
//   return new Promise((resolve) => {
//     if (image.complete !== true) {
//       image.onload = function () {
//         let { width, height } = window.getComputedStyle(image);
//         resolve(width, height);
//       };
//       image.onerror = function (e) {
//         let { width, height } = window.getComputedStyle(image);
//         resolve(width, height, e);
//       };
//     } else {
//       let { width, height } = window.getComputedStyle(image);
//       resolve(width, height);
//     }
//   });
// }